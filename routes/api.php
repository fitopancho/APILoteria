<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SorteoController;
use App\Http\Controllers\PokemonController;
use App\Http\Controllers\PokemonWebController;
use App\Http\Controllers\PokemonControllerNodo2;
use App\Http\Controllers\PokemonGetInfoController;
use App\Http\Controllers\UtilsController;
use App\Http\Controllers\RolDelayController;


//use App\Http\Middleware\ValidateToken;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/sorteo/id/{sorteo}', [SorteoController::class, 'getSorteoById']);
Route::get('/sorteo/actualizar', [SorteoController::class, 'actualizarSorteos']);
Route::get('/sorteo/pronostico', [SorteoController::class, 'PronosticoSorteos']);
Route::group(['middleware' => ['cors']], function () {
    Route::get('/sorteo/ultimo', [SorteoController::class, 'getUltimoSorteo']);
});

Route::get('/pokemon/list', [PokemonController::class, 'getListPokemon']);
Route::get('/pokemon/info/{pokemon}', [PokemonController::class, 'getBattleInfo']);
Route::get('/pokemon/type/{type}', [PokemonController::class, 'getListPokeType']);

Route::get('/nodo2/pokemon/list', [PokemonControllerNodo2::class, 'getListPokemon']);
Route::get('/nodo2/pokemon/info/{pokemon}', [PokemonControllerNodo2::class, 'getBattleInfo']);
Route::get('/nodo2/pokemon/type/{type}', [PokemonControllerNodo2::class, 'getListPokeType']);

Route::get('/web/pokemon/list/{pokemonName}', [PokemonWebController::class, 'getListPokemonByName']);
Route::post('/web/pokemon/list/', [PokemonWebController::class, 'findPokemon']);

//Route::get('/pokemon/all/{pokemonName}', [PokemonGetInfoController::class, 'getAll']);
//Route::get('/pokemon/v2/{pokemon}', [PokemonGetInfoController::class, 'getPoke']);
//Route::post('/pokemon/create/', [PokemonGetInfoController::class, 'createPoke']);
//Route::get('/pokemon/habilidades/', [PokemonGetInfoController::class, 'getHabilidades']);
//Route::get('/pokemon/actualizaNumeros/', [PokemonGetInfoController::class, 'getListPokemon']);

Route::get('/youtube/tags/{video}', [UtilsController::class, 'getTagsYoutube']);


