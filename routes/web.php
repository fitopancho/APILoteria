<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PokemonWebController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('listaPokemon');
});

Route::get('/pokemon/{pokemon}', [PokemonWebController::class, 'getPokemonBlade']);
