
<h1>API-FitoPanchoDev</h1><h5>2.1.0</h5>

## ¿Qué es?

<p>Es un conjunto de servicios creados por mi, en donde integro servicios creados por otros y servicios creador por mi.</p>
<p>servicios API-Loteria consta de un pequeño y basico SCRAPING y guardado de resultados en base de datos. Esto es utilizado para obtener los resultados de los sorteos disponibles en la página de loteria.cl. El fin de esta API no es más que academico y se utilizará para alimentar una APP movil android, que tambien tiene un fin academico.</p>

<p>servicio Pokemon utiliza la api https://pokeapi.co/ y recolecto informacion relevante para tomar decisiones en batallas de pokemon VGC.</p>

<p>Las API aqui expuestas están enfocadas en la funcionalidad. Requieren de mejoras, refactorizaciones y aplicacion de seguridad.</p>

## RolDelay

<p>Se añadio la plataforma Rol delay, una herramienta para acercar a los jugadores de rol que no tienen el mismo tiempo para reunirse y dedicarle el tiempo que se merece a este hermoso pasatiempos</p>
<p>Los endpoints no se publicaran aquí, pero si conoces Laravel podrás encontrarlos</p>

## AMBIENTE

- PHP 8.1.12
- Laravel 10.10
- MYSQL

## Servicios API-Loteria

- GET: http://127.0.0.1:8000/api/sorteo/ultimo Obtiene los resultados del ultimo sorteo en la página
- GET: http://127.0.0.1:8000/api/sorteo/id/{sorteo} Obtiene los resultados del sorteo (sorteo) en la página, hay un límite de 60 días para consultar
- GET: http://127.0.0.1:8000/api/sorteo/actualizar Actualiza la base de datos con los datos de los sorteos realizados (disponibles en la página)
- GET: http://127.0.0.1:8000/api/sorteo/pronostico Realiza un "pronostico" de apuesta, actualmente solo ordena de mayor a menor frecuencia los números

## Servicios Utils

- GET: http://127.0.0.1:8000/api/youtube/tags/{video} Obtiene los tags de un video de youtube, {video} = identificador del video

## Servicios Pokemon
- GET: http://127.0.0.1:8000/api/pokemon/info/{pokemon} Obtiene informacion relevante para pokemon VGC
- GET: http://127.0.0.1:8000/api/pokemon/list Obtiene listado (nombre) de todos los pokemon en base de datos
- GET: http://127.0.0.1:8000/api/pokemon/type/{type} Obtiene los pokemon del tipo seleccionado
- GET: http://127.0.0.1:8000/api/nodo2/pokemon/info/{pokemon} Obtiene informacion relevante para pokemon VGC
- GET: http://127.0.0.1:8000/api/nodo2/pokemon/list Obtiene listado (nombre) de todos los pokemon en base de datos
- GET: http://127.0.0.1:8000/api/nodo2/pokemon/type/{type} Obtiene los pokemon del tipo seleccionado

<p>Los siguientes endpoints han sido comentados del codigo ya que no están pulidos, ni tampoco responden a las buenas practicas de programación, dado que fueron creados solo para ser usados una vez, con el fin de almacenar los datos que usará la APP</p>

- GET: http://127.0.0.1:8000/api/pokemon/all/{pokemonName} Scrap que obtiene informacion relevante para pokemon VGC desde wikidex y pokeapi.co para luego almacenarla en la BD local
- GET: http://127.0.0.1:8000/api/pokemon/v2/{pokemon} Obtiene la informacion del pokemon desde la base local
- GET: http://127.0.0.1:8000/api/pokemon/habilidades Scrap que obtiene el efecto de las habilidades en arreglo
- POST: http://127.0.0.1:8000/api/pokemon/create Crea pokemon con ciertos parametros
- GET: http://127.0.0.1:8000/api/pokemon/actualizaNumeros/ Actualiza los numeros de la pokedex nacional en base de datos
