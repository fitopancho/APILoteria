<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\PokemonControllerNodo2;

class PokemonWebController extends Controller
{

    public function getListPokemonByName($pokemonName){

        try{
            $pokemon = DB::table('pokemon')->where('name',$pokemonName)->get();
            $listaPokemon = [];
            foreach($pokemon as $poke){
                array_push($listaPokemon, $poke->name);
            }

            return $listaPokemon;
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

    public function findPokemon(Request $request){

        $hpMin = $request->input('hpMin');
        $hpMax = $request->input('hpMax');
        $atkMin = $request->input('atkMin');
        $atkMax = $request->input('atkMax');
        $defMin = $request->input('defMin');
        $defMax = $request->input('defMax');
        $sAtkMin = $request->input('sAtkMin');
        $sAtkMax = $request->input('sAtkMax');
        $sDefMin = $request->input('sDefMin');
        $sDefMax = $request->input('sDefMax');
        $velMin = $request->input('velMin');
        $valMax = $request->input('valMax');

        $where = [];

        if( $hpMin != ''){
            $arrayHpMin = ["hp_base",">=",$hpMin];
            array_push($where, $arrayHpMin);
        }
        if( $hpMax != ''){
            $arrayHpMax = ["hp_base","<=",$hpMax];
            array_push($where, $arrayHpMax);
        }
        if( $atkMin != ''){
            $arrayAtkMin = ["attack_base",">=",$atkMin];
            array_push($where, $arrayAtkMin);
        }
        if( $atkMax != ''){
            $arrayAtkMax = ["attack_base","<=",$atkMax];
            array_push($where, $arrayAtkMax);
        }
        if( $defMin != ''){
            $arrayDefMin = ["defense_base",">=",$defMin];
            array_push($where, $arrayDefMin);
        }
        if( $defMax != ''){
            $arrayDefMax = ["defense_base","<=",$defMax];
            array_push($where, $arrayDefMax);
        }
        if( $sAtkMin != ''){
            $arraySAtkMin = ["s_attack_base",">=",$sAtkMin];
            array_push($where, $arraySAtkMin);
        }
        if( $sAtkMax != ''){
            $arraySAtkMax = ["s_attack_base","<=",$sAtkMax];
            array_push($where, $arraySAtkMax);
        }
        if( $sDefMin != ''){
            $arraySDefMin = ["s_defense_base",">=",$sDefMin];
            array_push($where, $arraySDefMin);
        }
        if( $sDefMax != ''){
            $arraySDefMax = ["s_defense_base","<=",$sDefMax];
            array_push($where, $arraySDefMax);
        }
        if( $velMin != ''){
            $arrayVelMin = ["speed_base",">=",$velMin];
            array_push($where, $arrayVelMin);
        }
        if( $valMax != ''){
            $arrayVelMax = ["speed_base","<=",$valMax];
            array_push($where, $arrayVelMax);
        }

        try{
            $pokemon = DB::table('pokemon')->where($where)->get();
            $listaPokemon = [];
            foreach($pokemon as $poke){
                array_push($listaPokemon, $poke->name);
            }

            return $listaPokemon;
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

    public function getPokemonBlade($pokemon){

        try{
            $info = (new PokemonControllerNodo2)->getBattleInfo($pokemon);
            $i = 0;
            foreach($info['stats'] as $stat){
                if($stat['nm_stat'] == 'hp'){
                    $info['stats'][$i]['nm_stat'] = 'PS';
                }
                if($stat['nm_stat'] == 'atk'){
                    $info['stats'][$i]['nm_stat'] = 'Ataque';
                }
                if($stat['nm_stat'] == 'def'){
                    $info['stats'][$i]['nm_stat'] = 'Defensa';
                }
                if($stat['nm_stat'] == 'spa'){
                    $info['stats'][$i]['nm_stat'] = 'Ataque Especial';
                }
                if($stat['nm_stat'] == 'spd'){
                    $info['stats'][$i]['nm_stat'] = 'Defensa Especial';
                }
                if($stat['nm_stat'] == 'spe'){
                    $info['stats'][$i]['nm_stat'] = 'Velocidad';
                }
                $i++;
            }

            
            return view('pokemon')->with('data', $info);
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }
    
    
    

}
