<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Storage;

class UtilsController extends Controller
{
    public function getTagsYoutube($video){

        try{
            $url = "https://www.youtube.com/watch?v=$video";
            $info = file_get_contents($url);

            preg_match('/<meta name="keywords" content="(.*?)">/s', $info, $tags);
            return explode(",",$tags[1]);

        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

}
