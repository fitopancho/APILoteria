<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Exception;

class SorteoController extends Controller
{

    public function getUltimoSorteo(){

        try{          
            $sorteo = 0;

            $resSorteo = $this->getSorteo($sorteo);

            return array(
                "cd_msg"=> 0,
                "msg"=> "OK",
                "sorteo" => $resSorteo
            ); 
        }catch(Exception $e){ 
            log::debug($e);
            return array(
                "cd_msg"=> -1,
                "msg"=>$e->getmessage(),
                "sorteo" => []
            );      
                    
        }
    }

    public function getSorteoById($sorteo){

        try{          
            $resSorteo = $this->getSorteo($sorteo);

            return array(
                "cd_msg"=> 0,
                "msg"=> "OK",
                "sorteo" => $resSorteo
            ); 
        }catch(Exception $e){ 
            log::debug($e);
            return array(
                "cd_msg"=> -1,
                "msg"=>$e->getmessage(),
                "sorteo" => []
            );      
                    
        }
    }

    public function getSorteo($sorteo){

        try{          
            $postdata = http_build_query(
                array(
                    'sorteo' => $sorteo,
                )
            );
            
            $opts = array('http' =>
                array(
                    'method'  => 'POST',
                    'header'  => 'Content-Type: application/x-www-form-urlencoded',
                    'content' => $postdata
                )
            );
            
            $context  = stream_context_create($opts);
            $pageLoteria = file_get_contents("https://www.loteria.cl/loterianet/resultados/kino/", false, $context);

            if(str_contains($pageLoteria, '500002')){
                return array();
            }
            preg_match('/<span id="fechaSorteo">(.*?)<\/span>/s', $pageLoteria, $dateMatch);

            preg_match('/<option selected>(.*?)<\/option>/s', $pageLoteria, $n_match);

            preg_match('/<ul class="numeros lineaBolitasKino pull-right">(.*?)<\/ul>/s', $pageLoteria, $match);

            $numerosSorteo = str_replace('<li><span class="bola bolita">', "", $match[1]);
            $numerosSorteo = str_replace('</span></li>', "", $numerosSorteo);
            $numerosSorteo = str_replace('<li class="break"></li>', "", $numerosSorteo);
            $numerosSorteo = str_replace("\n", "", $numerosSorteo);
            $numerosSorteo = trim($numerosSorteo);
            $numerosSorteo = str_replace(["\r\n", "\r", "\n", "\t"], '', $numerosSorteo);
            $numerosSorteo = trim($numerosSorteo);
            $numerosSorteo = explode(" ", $numerosSorteo);
            $arrayVacio = array(
                ""
            );
            $numerosSorteo=array_diff($numerosSorteo,$arrayVacio);

            $resNumerosSorteo = array(
                "n_sorteo" => $n_match[1],
                "num1" => $numerosSorteo[0],
                "num2" => $numerosSorteo[80],
                "num3" => $numerosSorteo[160],
                "num4" => $numerosSorteo[240],
                "num5" => $numerosSorteo[320],
                "num6" => $numerosSorteo[400],
                "num7" => $numerosSorteo[480],
                "num8" => $numerosSorteo[640],
                "num9" => $numerosSorteo[720],
                "num10" => $numerosSorteo[800],
                "num11" => $numerosSorteo[880],
                "num12" => $numerosSorteo[960],
                "num13" => $numerosSorteo[1040],
                "num14" => $numerosSorteo[1120],
                "fecha_sorteo" => $dateMatch[1]
            );
            
            return $resNumerosSorteo;
        }catch(Exception $e){ 
            log::debug($e);
            return array();           
                 
        }
    }

    public function actualizarSorteos(){

        try{     
            $r = DB::table('sorteos')->orderByDesc('n_sorteo')->limit(1)->get();//ultimo sorteo en BD    
            $sorteo = 0;
            $s = $this->getSorteoById($sorteo);
            
            if(count($r) == 0){
                //actualizo
                $ss = $this->storageSorteo($s);
                $resp_storage = $this->revisionSorteos($s, $ss);
            }else{
                if($r[0]->n_sorteo == $s['sorteo']['n_sorteo']){
                    //El ultimo sortero en BD es el mismo en la pagina
                    //no hay que actualizar
                    $resp_storage = array(
                        "cd_msg"=>0,
                        "msg" => "solicitud realizada con éxito"
                    );
                }else if($r[0]->n_sorteo < $s['sorteo']['n_sorteo']){
                    //hay que actualizar
    
                    $ss = $this->storageSorteo($s);
                    $resp_storage = $this->revisionSorteos($s, $ss);
                }else{
                    $resp_storage = array(
                        "cd_msg"=>0,
                        "msg" => "solicitud realizada con éxito"
                    );
                }
            }

            
            //log::debug($resp_storage);
            //if($resp_storage['cd_msg'] == 0 ){
                $cd_msg = 0;
                $msg = 'OK';
            /*}else{
                $cd_msg = 1;
                $msg = 'Puede que existan errores en el proceso';
            }*/
            return array(
                "cd_msg"=>$cd_msg,
                "msg" => $msg
            );
            

        }catch(Exception $e){ 
            log::debug($e);
            return array(
                "cd_msg"=>-1,
                "msg" => "error en la solicitud"
            );
                    
        }
    }

    public function revisionSorteos($sorteo, $ss){

        try{          
            if($ss["cd_msg"] == 0){
                $sorteo = $sorteo['sorteo']['n_sorteo']-1;
                $s = $this->getSorteoById($sorteo);
                if(count($s['sorteo'])>0){
                    $rs = DB::table('sorteos')->where('n_sorteo', $s['sorteo']['n_sorteo'])->get();
                    if(count($rs)==0 && count($s['sorteo']) > 0){
                        //si no existe en BD y sorteo trae datos
                        $ss = $this->storageSorteo($s);
                        $resp_storage = $this->revisionSorteos($s, $ss);
                    }else{
                        return array(
                            "cd_msg"=>0,
                            "msg" => "solicitud realizada con éxito"
                        );
                    }
                }else{
                    return array(
                        "cd_msg"=>0,
                        "msg" => "solicitud realizada con éxito"
                    );
                }
                
            }else{
                return array(
                    "cd_msg"=>0,
                    "msg" => "solicitud realizada con éxito"
                );
            }
        }catch(Exception $e){ 
            log::debug($e);
            return array(
                "cd_msg"=>-1,
                "msg" => "error en la solicitud"
            );
                    
        }
    }

    public function storageSorteo($sorteo){

        try{          
            $r = DB::insert(
                'insert into sorteos ( n_sorteo, fecha, n'.$sorteo['sorteo']['num1'].', n'.$sorteo['sorteo']['num2'].', n'.$sorteo['sorteo']['num3'].', n'.$sorteo['sorteo']['num4'].', n'.$sorteo['sorteo']['num5'].', n'.$sorteo['sorteo']['num6'].', n'.$sorteo['sorteo']['num7'].', n'.$sorteo['sorteo']['num8'].', n'.$sorteo['sorteo']['num9'].', n'.$sorteo['sorteo']['num10'].', n'.$sorteo['sorteo']['num11'].', n'.$sorteo['sorteo']['num12'].', n'.$sorteo['sorteo']['num13'].', n'.$sorteo['sorteo']['num14'].') values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                 [$sorteo['sorteo']['n_sorteo'], $sorteo['sorteo']['fecha_sorteo'],  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
            return array(
                "cd_msg" => 0
            );
        }catch(Exception $e){ 
            log::debug($e);
            return array(
                "cd_msg" => -1
            );
                    
        }
    }


    public function PronosticoSorteos(){

        try{          
            
            $rs = DB::table('sorteos')->limit(25)->orderBy('n_sorteo')->get();

            $num1=0;

            $num2=0;
            $num3=0;
            $num4=0;
            $num5=0;
            $num6=0;
            $num7=0;
            $num8=0;
            $num9=0;
            $num10=0;
            $num11=0;
            $num12=0;
            $num13=0;
            $num14=0;
            $num15=0;
            $num16=0;
            $num17=0;
            $num18=0;
            $num19=0;
            $num20=0;
            $num21=0;
            $num22=0;
            $num23=0;
            $num24=0;
            $num25=0;

            foreach($rs as $row){
                if($row->n01){
                    $num1++;
                }
                if($row->n02){
                    $num2++;
                }
                if($row->n03){
                    $num3++;
                }
                if($row->n04){
                    $num4++;
                }
                if($row->n05){
                    $num5++;
                }
                if($row->n06){
                    $num6++;
                }
                if($row->n07){
                    $num7++;
                }
                if($row->n08){
                    $num8++;
                }
                if($row->n09){
                    $num9++;
                }
                if($row->n10){
                    $num10++;
                }
                if($row->n11){
                    $num11++;
                }
                if($row->n12){
                    $num12++;
                }
                if($row->n13){
                    $num13++;
                }
                if($row->n14){
                    $num14++;
                }
                if($row->n15){
                    $num15++;
                }
                if($row->n16){
                    $num16++;
                }
                if($row->n17){
                    $num17++;
                }
                if($row->n18){
                    $num18++;
                }
                if($row->n19){
                    $num19++;
                }
                if($row->n20){
                    $num20++;
                }
                if($row->n21){
                    $num21++;
                }
                if($row->n22){
                    $num22++;
                }
                if($row->n23){
                    $num23++;
                }
                if($row->n24){
                    $num24++;
                }
                if($row->n25){
                    $num25++;
                }
                
            }

            $data = array(
                "cant_sorteos" => count($rs),
                "numeros" => array(
                    array(
                        "numero" => 1,
                        "repeticiones"=>$num1
                    ),
                    array(
                        "numero" => 2,
                        "repeticiones"=>$num2
                    ),
                    array(
                        "numero" => 3,
                        "repeticiones"=>$num3
                    ),
                    array(
                        "numero" => 4,
                        "repeticiones"=>$num4
                    ),
                    array(
                        "numero" => 5,
                        "repeticiones"=>$num5
                    ),
                    array(
                        "numero" => 6,
                        "repeticiones"=>$num6
                    ),
                    array(
                        "numero" => 7,
                        "repeticiones"=>$num7
                    ),
                    array(
                        "numero" => 8,
                        "repeticiones"=>$num8
                    ),
                    array(
                        "numero" => 9,
                        "repeticiones"=>$num9
                    ),
                    array(
                        "numero" => 10,
                        "repeticiones"=>$num10
                    ),
                    array(
                        "numero" => 11,
                        "repeticiones"=>$num11
                    ),
                    array(
                        "numero" => 12,
                        "repeticiones"=>$num12
                    ),
                    array(
                        "numero" => 13,
                        "repeticiones"=>$num13
                    ),
                    array(
                        "numero" => 14,
                        "repeticiones"=>$num14
                    ),
                    array(
                        "numero" => 15,
                        "repeticiones"=>$num15
                    ),
                    array(
                        "numero" => 16,
                        "repeticiones"=>$num16
                    ),
                    array(
                        "numero" => 17,
                        "repeticiones"=>$num17
                    ),
                    array(
                        "numero" => 18,
                        "repeticiones"=>$num18
                    ),
                    array(
                        "numero" => 19,
                        "repeticiones"=>$num19
                    ),
                    array(
                        "numero" => 20,
                        "repeticiones"=>$num20
                    ),
                    array(
                        "numero" => 21,
                        "repeticiones"=>$num21
                    ),
                    array(
                        "numero" => 22,
                        "repeticiones"=>$num22
                    ),
                    array(
                        "numero" => 23,
                        "repeticiones"=>$num23
                    ),
                    array(
                        "numero" => 24,
                        "repeticiones"=>$num24
                    ),
                    array(
                        "numero" => 25,
                        "repeticiones"=>$num25
                    )
                )
                
            );
            $temp = null;
            for ( $i = 0; $i < count($data["numeros"]); $i++ )
                for ( $j = 0; $j < count($data["numeros"]); $j++ ){
                    if ($data["numeros"][$i]["repeticiones"] > $data["numeros"][$j]["repeticiones"])
                    {
                       $temp = $data["numeros"][$i];
                       $data["numeros"][$i] = $data["numeros"][$j];
                       $data["numeros"][$j] = $temp;
                    }
                    
                }

            return $data;
                    
        }catch(Exception $e){ 
            log::debug($e);
            return array(
                "cd_msg"=>-1,
                "msg" => "error en la solicitud"
            );
                    
        }
    }

}
