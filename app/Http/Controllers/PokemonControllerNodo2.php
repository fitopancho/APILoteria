<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Storage;

class PokemonControllerNodo2 extends Controller
{

    public function getBattleInfo($pokemon){

        try{          
            $pokemon = DB::table('pokemon')->where('name',ucwords($pokemon))->get();
  
            $type1 = $this->getType($pokemon[0]->type1);
            $type2 = "";
            if($pokemon[0]->type2 != ""){
                $type2 = $this->getType($pokemon[0]->type2);
            }
            $types = array();
            array_push($types, $type1);
            if($type2 != ""){
                array_push($types, $type2);
            }

            $stats = $this->getStats($pokemon);
            $habilidades = $this->getHabilidades($pokemon);
            $debilidades = $this->getDebilidades($pokemon);
            $sprite_url = "https://fitopanchodev.cl/laravel8/public/assets/pokemon/".$pokemon[0]->name.".png";

            return array(
                "tipo" => $types,
                "stats" => $stats,
                "habilidades" => $habilidades,
                "debilidades" => $debilidades,
                "sprite" => $sprite_url
                
            );
            
        }catch(Exception $e){ 
            log::debug($e);
         
        }
    }

    public function getType($type){

        try{
            return DB::table('tipos')->where('nombre_es',$type)->get()[0];
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

    public function getTypeEn($type){

        try{
            return DB::table('tipos')->where('nombre',$type)->get()[0];
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }


    public function getStats($pokemon){

        try{
            $stats = array(
                array(
                    "nm_stat" => "hp",
                    "val_base" => $pokemon[0]->hp_base,
                    "val_min" => $pokemon[0]->hp_min,
                    "val_max" => $pokemon[0]->hp_max
                ),
                array(
                    "nm_stat" => "atk",
                    "val_base" => $pokemon[0]->attack_base,
                    "val_min" => $pokemon[0]->attack_min,
                    "val_max" => $pokemon[0]->attack_max
                ),
                array(
                    "nm_stat" => "def",
                    "val_base" => $pokemon[0]->defense_base,
                    "val_min" => $pokemon[0]->defense_min,
                    "val_max" => $pokemon[0]->defense_max
                ),
                array(
                    "nm_stat" => "spa",
                    "val_base" => $pokemon[0]->s_attack_base,
                    "val_min" => $pokemon[0]->s_attack_min,
                    "val_max" => $pokemon[0]->s_attack_max
                ),
                array(
                    "nm_stat" => "spd",
                    "val_base" => $pokemon[0]->s_defense_base,
                    "val_min" => $pokemon[0]->s_defense_min,
                    "val_max" => $pokemon[0]->s_defense_max
                ),
                array(
                    "nm_stat" => "spe",
                    "val_base" => $pokemon[0]->speed_base,
                    "val_min" => $pokemon[0]->speed_min,
                    "val_max" => $pokemon[0]->speed_max
                ),
            );
            return $stats;
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

    public function getHabilidades($pokemon){

        try{
            $habs = array();
            $hab = $this->getHabInfo($pokemon[0]->ability1);
            array_push($habs, $hab);
            if($pokemon[0]->ability2 != ''){
                $hab = $this->getHabInfo($pokemon[0]->ability2);
                array_push($habs, $hab);
            }
            if($pokemon[0]->hiddenAbility != ''){
                $hab = $this->getHabInfo($pokemon[0]->hiddenAbility);
                array_push($habs, $hab);
            }
            return $habs;
        }catch(Exception $e){ 
            log::debug($e);
       
        }
    }

    public function getHabInfo($habilidad){

        try{
            $habilidad = DB::table('habilidades')->where('name',$habilidad)->get();
            $habilidad = array(
                "nombre" => $habilidad[0]->name,
                "efecto" => $habilidad[0]->effect
            );
            return $habilidad;
        }catch(Exception $e){ 
            log::debug($e);
       
        }
    }

    public function getDebilidades($pokemon){

        try{
            return array(
                "cero"=> $this->explodeDebilidades($pokemon[0]->weakx0),
                "cuarto"=> $this->explodeDebilidades($pokemon[0]->weakx1_4),
                "medio"=> $this->explodeDebilidades($pokemon[0]->weakx1_2),
                "doble"=> $this->explodeDebilidades($pokemon[0]->weakx2),
                "cuatro"=> $this->explodeDebilidades($pokemon[0]->weakx4),
                "normal"=> $this->explodeDebilidades($pokemon[0]->weakx1)
            );
        }catch(Exception $e){ 
            log::debug($e);
             
                    
        }
    }

    public function explodeDebilidades($tipos){

        try{
            $debArray = array();
            $array = explode(",", $tipos);
            if($array[0] == ''){
                $array = [];
            }
            foreach($array as $debString){
                array_push($debArray, $this->getTypeEn($debString));
            }
            return $debArray;
        }catch(Exception $e){ 
            log::debug($e);
             
                    
        }
    }



    public function getListPokemon(){

        try{
            $pokemon = DB::table('pokemon')->orderBy('nPoke', 'asc')->get();
            $listaPokemon = [];
            foreach($pokemon as $poke){
                array_push($listaPokemon, $poke->name);
            }

            return $listaPokemon;
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }
    
    
    public function getListPokeType($type){

        try{
            $type = $this->getTypeEn($type);
            $type = $type->nombre_es;
            $pokemon = DB::table('pokemon')->where('type1',$type)->orWhere('type2',$type)->orderBy('nPoke', 'asc')->get();
            $pokes = array();
            foreach($pokemon as $poke){
                array_push($pokes, array(
                    "nombre" => $poke->name,
                    "tipo1" => $poke->type1,
                    "tipo2" => $poke->type2
                ));
            }
            return array(
                "nombre" => $type,
                "pokemon" => $pokes

            );
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

}
