<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Storage;

class PokemonController extends Controller
{

    public function getBattleInfo($pokemon){

        try{          
            $pokemon = DB::table('pokemon')->where('name',ucwords($pokemon))->get();

            $type1 = $this->getType($pokemon[0]->type1);
            $type2 = $this->getType($pokemon[0]->type2);
            $types = array();
            array_push($types, $type1);
            if(count($type2) != 0){
                array_push($types, $type2);
            }

            $stats = $this->getStats($pokemon);
            $habilidades = $this->getHabilidades($pokemon);
            $debilidades = $this->getDebilidades($pokemon);
            $sprite_url = "https://fitopanchodev.cl/laravel8/public/assets/pokemon/".$pokemon[0]->name.".png";

            return array(
                "tipo" => $types,
                "stats" => $stats,
                "habilidades" => $habilidades,
                "debilidades" => $debilidades,
                "sprite" => $sprite_url
                
            );
            
        }catch(Exception $e){ 
            log::debug($e);
         
        }
    }

    public function getType($type){

        try{
            
            switch ($type) {
                case "planta":
                    $type = array(
                        "nombre" => 'grass',
                        "nombre_es" => "planta",
                        "color" => "#3CA024",
                        "id" => 12
                    );
                    break;
                case "fuego":
                    $type = array(
                        "nombre" => 'fire',
                        "nombre_es" => "fuego",
                        "color" => "#E42324",
                        "id" => 10
                    );
                    break;
                case "agua":
                    $type = array(
                        "nombre" => 'water',
                        "nombre_es" => "agua",
                        "color" => "#247FEC",
                        "id" => 11
                    );
                    break;
                case "bicho":
                    $type = array(
                        "nombre" => 'bug',
                        "nombre_es" => "bicho",
                        "color" => "#889711",
                        "id" => 7
                    );
                    break;
                case "normal":
                    $type = array(
                        "nombre" => 'normal',
                        "nombre_es" => "normal",
                        "color" => "#A0A2A0",
                        "id" => 1
                    );
                    break;
                case "veneno":
                    $type = array(
                        "nombre" => 'poison',
                        "nombre_es" => "veneno",
                        "color" => "#893BC0",
                        "id" => 4
                    );
                    break;
                case "eléctrico":
                    $type = array(
                        "nombre" => 'electric',
                        "nombre_es" => "eléctrico",
                        "color" => "#ECB600",
                        "id" => 13
                    );
                    break;
                case "tierra":
                    $type = array(
                        "nombre" => 'ground',
                        "nombre_es" => "tierra",
                        "color" => "#894B19",
                        "id" => 5
                    );
                    break;
                case "hada":
                    $type = array(
                        "nombre" => 'fairy',
                        "nombre_es" => "hada",
                        "color" => "#E169E1",
                        "id" => 18
                    );
                    break;
                case "lucha":
                    $type = array(
                        "nombre" => 'fighting',
                        "nombre_es" => "lucha",
                        "color" => "#FC7F00",
                        "id" => 2
                    );
                    break;
                case "psíquico":
                    $type = array(
                        "nombre" => 'psychic',
                        "nombre_es" => "psíquico",
                        "color" => "#E13B73",
                        "id" => 14
                    );
                    break;
                case "roca":
                    $type = array(
                        "nombre" => 'rock',
                        "nombre_es" => "roca",
                        "color" => "#A39D78",
                        "id" => 6
                    );
                    break;
                case "fantasma":
                    $type = array(
                        "nombre" => 'ghost',
                        "nombre_es" => "fantasma",
                        "color" => "#6E3E6E",
                        "id" => 8
                    );
                    break;
                case "hielo":
                    $type = array(
                        "nombre" => 'ice',
                        "nombre_es" => "hielo",
                        "color" => "#39CCF0",
                        "id" => 15
                    );
                    break;
                case "dragón":
                    $type = array(
                        "nombre" => 'dragon',
                        "nombre_es" => "dragón",
                        "color" => "#4C5DDA",
                        "id" => 16
                    );
                    break;
                case "siniestro":
                    $type = array(
                        "nombre" => 'dark',
                        "nombre_es" => "siniestro",
                        "color" => "#4C3C3A",
                        "id" => 17
                    );
                    break;
                case "acero":
                    $type = array(
                        "nombre" => 'steel',
                        "nombre_es" => "acero",
                        "color" => "#60A2B9",
                        "id" => 9
                    );
                    break;
                case "volador":
                    $type = array(
                        "nombre" => 'flying',
                        "nombre_es" => "volador",
                        "color" => "#7DB3E6",
                        "id" => 3
                    );
                    break;
                default :
                    $type = array();
            }

            return $type;
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

    public function getTypeEn($type){

        try{
            
            switch ($type) {
                case "grass":
                    $type = array(
                        "nombre" => 'grass',
                        "nombre_es" => "planta",
                        "color" => "#3CA024",
                        "id" => 12
                    );
                    break;
                case "fire":
                    $type = array(
                        "nombre" => 'fire',
                        "nombre_es" => "fuego",
                        "color" => "#E42324",
                        "id" => 10
                    );
                    break;
                case "water":
                    $type = array(
                        "nombre" => 'water',
                        "nombre_es" => "agua",
                        "color" => "#247FEC",
                        "id" => 11
                    );
                    break;
                case "bug":
                    $type = array(
                        "nombre" => 'bug',
                        "nombre_es" => "bicho",
                        "color" => "#889711",
                        "id" => 7
                    );
                    break;
                case "normal":
                    $type = array(
                        "nombre" => 'normal',
                        "nombre_es" => "normal",
                        "color" => "#A0A2A0",
                        "id" => 1
                    );
                    break;
                case "poison":
                    $type = array(
                        "nombre" => 'poison',
                        "nombre_es" => "veneno",
                        "color" => "#893BC0",
                        "id" => 4
                    );
                    break;
                case "electric":
                    $type = array(
                        "nombre" => 'electric',
                        "nombre_es" => "eléctrico",
                        "color" => "#ECB600",
                        "id" => 13
                    );
                    break;
                case "ground":
                    $type = array(
                        "nombre" => 'ground',
                        "nombre_es" => "tierra",
                        "color" => "#894B19",
                        "id" => 5
                    );
                    break;
                case "fairy":
                    $type = array(
                        "nombre" => 'fairy',
                        "nombre_es" => "hada",
                        "color" => "#E169E1",
                        "id" => 18
                    );
                    break;
                case "fighting":
                    $type = array(
                        "nombre" => 'fighting',
                        "nombre_es" => "lucha",
                        "color" => "#FC7F00",
                        "id" => 2
                    );
                    break;
                case "psychic":
                    $type = array(
                        "nombre" => 'psychic',
                        "nombre_es" => "psíquico",
                        "color" => "#E13B73",
                        "id" => 14
                    );
                    break;
                case "rock":
                    $type = array(
                        "nombre" => 'rock',
                        "nombre_es" => "roca",
                        "color" => "#A39D78",
                        "id" => 6
                    );
                    break;
                case "ghost":
                    $type = array(
                        "nombre" => 'ghost',
                        "nombre_es" => "fantasma",
                        "color" => "#6E3E6E",
                        "id" => 8
                    );
                    break;
                case "ice":
                    $type = array(
                        "nombre" => 'ice',
                        "nombre_es" => "hielo",
                        "color" => "#39CCF0",
                        "id" => 15
                    );
                    break;
                case "dragon":
                    $type = array(
                        "nombre" => 'dragon',
                        "nombre_es" => "dragón",
                        "color" => "#4C5DDA",
                        "id" => 16
                    );
                    break;
                case "dark":
                    $type = array(
                        "nombre" => 'dark',
                        "nombre_es" => "siniestro",
                        "color" => "#4C3C3A",
                        "id" => 17
                    );
                    break;
                case "steel":
                    $type = array(
                        "nombre" => 'steel',
                        "nombre_es" => "acero",
                        "color" => "#60A2B9",
                        "id" => 9
                    );
                    break;
                case "flying":
                    $type = array(
                        "nombre" => 'flying',
                        "nombre_es" => "volador",
                        "color" => "#7DB3E6",
                        "id" => 3
                    );
                    break;
                default :
                    $type = array();
            }

            return $type;
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }


    public function getStats($pokemon){

        try{
            $stats = array(
                array(
                    "nm_stat" => "hp",
                    "val_base" => $pokemon[0]->hp_base,
                    "val_min" => $pokemon[0]->hp_min,
                    "val_max" => $pokemon[0]->hp_max
                ),
                array(
                    "nm_stat" => "atk",
                    "val_base" => $pokemon[0]->attack_base,
                    "val_min" => $pokemon[0]->attack_min,
                    "val_max" => $pokemon[0]->attack_max
                ),
                array(
                    "nm_stat" => "def",
                    "val_base" => $pokemon[0]->defense_base,
                    "val_min" => $pokemon[0]->defense_min,
                    "val_max" => $pokemon[0]->defense_max
                ),
                array(
                    "nm_stat" => "spa",
                    "val_base" => $pokemon[0]->s_attack_base,
                    "val_min" => $pokemon[0]->s_attack_min,
                    "val_max" => $pokemon[0]->s_attack_max
                ),
                array(
                    "nm_stat" => "spd",
                    "val_base" => $pokemon[0]->s_defense_base,
                    "val_min" => $pokemon[0]->s_defense_min,
                    "val_max" => $pokemon[0]->s_defense_max
                ),
                array(
                    "nm_stat" => "spe",
                    "val_base" => $pokemon[0]->speed_base,
                    "val_min" => $pokemon[0]->speed_min,
                    "val_max" => $pokemon[0]->speed_max
                ),
            );
            return $stats;
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

    public function getHabilidades($pokemon){

        try{
            $habs = array();
            $hab = $this->getHabInfo($pokemon[0]->ability1);
            array_push($habs, $hab);
            if($pokemon[0]->ability2 != ''){
                $hab = $this->getHabInfo($pokemon[0]->ability2);
                array_push($habs, $hab);
            }
            if($pokemon[0]->hiddenAbility != ''){
                $hab = $this->getHabInfo($pokemon[0]->hiddenAbility);
                array_push($habs, $hab);
            }
            return $habs;
        }catch(Exception $e){ 
            log::debug($e);
       
        }
    }

    public function getHabInfo($habilidad){

        try{
            $habilidad = DB::table('habilidades')->where('name',$habilidad)->get();
            $habilidad = array(
                "nombre" => $habilidad[0]->name,
                "efecto" => $habilidad[0]->effect
            );
            return $habilidad;
        }catch(Exception $e){ 
            log::debug($e);
       
        }
    }

    public function getDebilidades($pokemon){

        try{
            return array(
                "cero"=> $this->explodeDebilidades($pokemon[0]->weakx0),
                "cuarto"=> $this->explodeDebilidades($pokemon[0]->weakx1_4),
                "medio"=> $this->explodeDebilidades($pokemon[0]->weakx1_2),
                "doble"=> $this->explodeDebilidades($pokemon[0]->weakx2),
                "cuatro"=> $this->explodeDebilidades($pokemon[0]->weakx4),
                "normal"=> $this->explodeDebilidades($pokemon[0]->weakx1)
            );
        }catch(Exception $e){ 
            log::debug($e);
             
                    
        }
    }

    public function explodeDebilidades($tipos){

        try{
            $array = explode(",", $tipos);
            if($array[0] == ''){
                $array = [];
            }
            return $array;
        }catch(Exception $e){ 
            log::debug($e);
             
                    
        }
    }



    public function getListPokemon(){

        try{
            $pokemon = DB::table('pokemon')->orderBy('nPoke', 'asc')->get();
            $listaPokemon = [];
            foreach($pokemon as $poke){
                array_push($listaPokemon, $poke->name);
            }

            return $listaPokemon;
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }
    
    
    public function getListPokeType($type){

        try{
            $type = $this->getTypeEn($type);
            $type = $type['nombre_es'];
            $pokemon = DB::table('pokemon')->where('type1',$type)->orWhere('type2',$type)->orderBy('nPoke', 'asc')->get();
            $pokes = array();
            foreach($pokemon as $poke){
                array_push($pokes, array(
                    "nombre" => $poke->name,
                    "tipo1" => $poke->type1,
                    "tipo2" => $poke->type2
                ));
            }
            return array(
                "nombre" => $type,
                "pokemon" => $pokes

            );
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

}
