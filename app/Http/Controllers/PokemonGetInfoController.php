<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Storage;

class PokemonGetInfoController extends Controller
{

   /* public function getAll($pokemonName){

        try{  
            ini_set('max_execution_time', 3600);        
            $url = "https://www.wikidex.net/wiki/Zorua_de_Hisui";
            $urlPokeAPI = "pokemon/zorua-hisui";
            $info = $this->getInfo($url);

            //_________________________
            $string = $info;
            $dom = new \DOMDocument();
            @$dom->loadHTML(html_entity_decode($string));

            //obtener nombre
            $spanName = $dom->getElementById('firstHeading')->childNodes[0];
            $innerName = $spanName->childNodes[0];
            $name = $dom->saveHtml($innerName);

            //obtener tipos
            $tipo1 = "";
            $tipo2 = "";
            preg_match('/<tr title="Tipos a los que pertenece">(.*?)<\/tr>/s', $string, $TiposBruto);
            preg_match('/title="Tipo (.*?)"/s', $TiposBruto[1], $tipo1);
            $tipo1 = $tipo1[1];
            if(str_contains($TiposBruto[1], '</a> <a')){
                $tipo2 = explode("</a> <a", $TiposBruto[1]);
                preg_match('/title="Tipo (.*?)"/s', $tipo2[1], $tipo2);
                $tipo2 = $tipo2[1];
            }

            //obtener Habilidades
            $habilidad1 = "";
            $habilidad2 = "";
            preg_match('/<tr title="Habilidades que puede conocer">(.*?)<\/tr>/s', $string, $HabsBruto);
            preg_match('/<td>(.*?)<\/td>/s', $HabsBruto[1], $HabsBruto);
            preg_match('/title="(.*?)"/s', $HabsBruto[1], $habilidad1);
            $habilidad1 = $habilidad1[1];
            if(str_contains($HabsBruto[1], 'a><br')){
                $habilidad2 = explode("a><br", $HabsBruto[1]);
                preg_match('/title="(.*?)"/s', $habilidad2[1], $habilidad2);
                $habilidad2 = $habilidad2[1];
            }

            //obtener Habilidad Oculta
            $habilidadO = "";
            if(preg_match('/<tr title="Habilidad oculta">(.*?)<\/tr>/s', $string, $HabsBruto) == 1){
                preg_match('/<tr title="Habilidad oculta">(.*?)<\/tr>/s', $string, $HabsBruto);
                preg_match('/<td>(.*?)<\/td>/s', $HabsBruto[1], $HabsBruto);
                preg_match('/title="(.*?)"/s', $HabsBruto[1], $habilidadO);
                $habilidadO = $habilidadO[1];
            }
            
            //obtener debilidades
            try{
                
                $info = $this->getInfoApi($urlPokeAPI);
                $debilidades = $this->getDebilidades($info->types);
            }catch(Exception $e){
                $debilidades = [];
            }
           

            //obtener stats
            try{
                $stats = $this->getStats($info->stats);
            }catch(Exception $e){
                $stats = [];
            }

            //obtener imagen
            try{
                $url_img = "";
                preg_match('/<img alt="Ilustración de (.*?)>/s', $string, $imgBruto);
                preg_match('/src="(.*?)" /s', $imgBruto[1], $imgBruto);
                $url_img =  $imgBruto[1];
            }catch(Exception $e){
                $url_img = '';
            }

            $next = '';
            if(!str_contains($next, "Ninguno")){
                preg_match('/Nacional<\/a><\/div>(.*?)<\/div>/s', $string, $next);
                preg_match('/<\/span>(.*?)<\/a>/s', $next[0], $next);
                preg_match('/">(.*?)<\/a>/s', $next[0], $next);
                $next = $next[1];
            }
            
            //_________________________


            $poke = array(
                "name" => $name,
                "types" =>array(
                    "type1" => $tipo1,
                    "type2" => $tipo2
                ),
                "abilities" =>array(
                    "ability1" => $habilidad1,
                    "ability2" => $habilidad2,
                    "hiddenAbility" => $habilidadO
                ),
                "weakness" => $debilidades,
                "stats" => $stats,
                "url_img" => $url_img,
                "next" => $next
            );
            Storage::disk('local')->put($poke["name"].".png", file_get_contents($poke['url_img']));
            $storagePoke = $this->almacenaPoke($poke);
            //if($poke['next'] != ''){
              //  sleep(2);
               // $resp = $this->getAll($poke['next']);
            //}
            return $poke;
        }catch(Exception $e){ 
            log::debug($e);
         
        }
    }

    public function almacenaPoke($poke){

        try{

            DB::insert(
                'insert into pokemon ( name, type1, type2, ability1, ability2, hiddenAbility, weakx0, weakx1_4, weakx1_2, weakx1, weakx2, weakx4, hp_base, hp_max, hp_min, attack_base, attack_max, attack_min, defense_base, defense_max, defense_min, s_attack_base, s_attack_max, s_attack_min, s_defense_base, s_defense_max, s_defense_min, speed_base, speed_max, speed_min) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                 [
                    $poke['name'],$poke['types']['type1'],$poke['types']['type2'],$poke['abilities']['ability1'],$poke['abilities']['ability2'],$poke['abilities']['hiddenAbility'],
                     implode(",", $poke['weakness']['cero']),implode(",", $poke['weakness']['cuarto']),implode(",", $poke['weakness']['medio']),implode(",", $poke['weakness']['normal']),
                    implode(",", $poke['weakness']['doble']),implode(",", $poke['weakness']['cuatro']),$poke['stats'][0]['valor_base'],$poke['stats'][0]['valor_max'],$poke['stats'][0]['valor_min'],
                    $poke['stats'][1]['valor_base'],$poke['stats'][1]['valor_max'],$poke['stats'][1]['valor_min'],
                    $poke['stats'][2]['valor_base'],$poke['stats'][2]['valor_max'],$poke['stats'][2]['valor_min'],
                    $poke['stats'][3]['valor_base'],$poke['stats'][3]['valor_max'],$poke['stats'][3]['valor_min'],
                    $poke['stats'][4]['valor_base'],$poke['stats'][4]['valor_max'],$poke['stats'][4]['valor_min'],
                    $poke['stats'][5]['valor_base'],$poke['stats'][5]['valor_max'],$poke['stats'][5]['valor_min'],
                ]);
            return "ok";
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

    public function getPoke($poke){

        try{

            $poke = DB::table('pokemon')->where('name',ucwords($poke))->get();
            return $poke;
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

    

    public function getInfo($url){

        try{
            $urlBase = "";
            $info = file_get_contents($urlBase.$url);
            return $info;
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

    public function getInfoApi($url){

        try{
            $urlBase = "https://pokeapi.co/api/v2/";
            $info = json_decode(file_get_contents($urlBase.$url));
            return $info;
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

    public function getStats($stats){

        try{
            $arrStats = array();
            foreach($stats as $stat){
                if($stat->stat->name == 'hp'){
                    $valmax = $stat->base_stat + 107;
                    $valmin = $stat->base_stat + 60;
                    $valbase = $stat->base_stat;
                }else{
                    $valmax = intval(($stat->base_stat + 52)*1.1);
                    $valmin = intval(($stat->base_stat + 5)*0.9);
                    $valbase = $stat->base_stat;
                }
                array_push($arrStats, array(
                    "nm_stat" => $stat->stat->name,
                    "valor_base" => $valbase,
                    "valor_max" => $valmax,
                    "valor_min" => $valmin
                ));
            }
            return $arrStats;
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

    public function getDebilidades($tipos){

        try{
            $arrDeb = array();
            $tiposArray = ['steel', 'water', 'bug', 'dragon', 'electric', 'ghost', 'fire', 'fairy', 'ice', 'fighting', 'normal', 'grass', 'psychic', 'rock', 'dark', 'ground', 'poison', 'flying'];
            foreach($tipos as $tipo){
                $debPorTipo = array();
                $dobleDaño = array();
                $mitadDaño = array();
                $ceroDaño = array();
                $debilidades = json_decode(file_get_contents($tipo->type->url));
                
                foreach($debilidades->damage_relations->double_damage_from as $debilidad){  
                    array_push($dobleDaño, $debilidad->name);
                }
                foreach($debilidades->damage_relations->half_damage_from as $debilidad){  
                    array_push($mitadDaño, $debilidad->name);
                }
                foreach($debilidades->damage_relations->no_damage_from as $debilidad){  
                    array_push($ceroDaño, $debilidad->name);
                }
                array_push($debPorTipo, $dobleDaño, $mitadDaño, $ceroDaño);
                array_push($arrDeb, $debPorTipo);
            }

            $porCuatro = [];
            $porDos = [];
            $normal = [];
            $laMitad = [];
            $unCuarto = [];
            $cero = [];
            
            foreach($tiposArray as $ta){
                $val1 = 1;
                $val2 = 1;
                $total = 1;
                if(in_array($ta,$arrDeb[0][0])){
                    $val1 = 2;
                }else if(in_array($ta,$arrDeb[0][1])){
                    $val1 = 0.5;
                }else if(in_array($ta,$arrDeb[0][2])){
                    $val1 = 0;
                }
                if(count($tipos) == 2){
                    if(in_array($ta,$arrDeb[1][0])){
                        $val2 = 2;
                    }else if(in_array($ta,$arrDeb[1][1])){
                        $val2 = 0.5;
                    }else if(in_array($ta,$arrDeb[1][2])){
                        $val2 = 0;
                    }
                }

                $total = $val1*$val2;

                switch ($total) {
                    case 0:
                        array_push($cero, $ta);
                        break;
                    case 0.5:
                        array_push($laMitad, $ta);
                        break;
                    case 0.25:
                        array_push($unCuarto, $ta);
                        break;
                    case 2:
                        array_push($porDos, $ta);
                        break;
                    case 4:
                        array_push($porCuatro, $ta);
                        break;
                    default:
                        array_push($normal, $ta);
                        break;
                }
            }
            
            return array(
                "cero"=>$cero,
                "cuarto"=>$unCuarto,
                "medio"=>$laMitad,
                "doble"=>$porDos,
                "cuatro"=>$porCuatro,
                "normal"=>$normal
            );
        }catch(Exception $e){ 
            log::debug($e);
             
                    
        }
    }

    public function createPoke(Request $request){

        try{
            $tipo1 = $request->tipo1;
            $tipo2 = $request->tipo2;
            $habilidad1 = $request->habilidad1;
            $habilidad2 = $request->habilidad2;
            $habilidadO = $request->habilidadO;
            if(empty($tipo1) ){
                $tipo1 = '';
            }
            if(empty($tipo2 )){
                $tipo2 = '';
            }
            if(empty($habilidad1 )){
                $habilidad1 = '';
            }
            if(empty($habilidad2 )){
                $habilidad2 = '';
            }
            if(empty($habilidadO )){
                $habilidadO = '';
            }

            try{
                $url = "pokemon/rotom-mow";
                $info = $this->getInfoApi($url);
                $debilidades = $this->getDebilidades($info->types);
            }catch(Exception $e){
                $debilidades = [];
            }
           

            //obtener stats
            try{
                $stats = $this->getStats($info->stats);
            }catch(Exception $e){
                $stats = [];
            }

            $poke = array(
                "name" => 'Rotom_corte',
                "types" =>array(
                    "type1" => $tipo1,
                    "type2" => $tipo2
                ),
                "abilities" =>array(
                    "ability1" => $habilidad1,
                    "ability2" => $habilidad2,
                    "hiddenAbility" => $habilidadO
                ),
                "weakness" => $debilidades,
                "stats" => $stats,
            );

            $storagePoke = $this->almacenaPoke($poke);
            return $poke;
            
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

    public function getHabilidades(){

        try{
            ini_set('max_execution_time', 3600);  
            $habilidades = [
                'Espesura'
                ,'Mar llamas'
                ,'Torrente'
                ,'Polvo escudo'
                ,'Mudar'
                ,'Ojo compuesto'
                ,'Enjambre'
                ,'Vista lince'
                ,'Fuga'
                ,'Intimidación'
                ,'Electricidad estática'
                ,'Velo arena'
                ,'Punto tóxico'
                ,'Gran encanto'
                ,'Absorbe fuego'
                ,'Fuerza mental'
                ,'Clorofila'
                ,'Efecto espora'
                ,'Recogida'
                ,'Flexibilidad'
                ,'Humedad'
                ,'Espíritu vital'
                ,'Absorbe agua'
                ,'Sincronía'
                ,'Agallas'
                ,'Cuerpo puro'
                ,'Cabeza roca'
                ,'Despiste'
                ,'Imán (habilidad)'
                ,'Sebo'
                ,'Hedor'
                ,'Caparazón'
                ,'Levitación'
                ,'Insomnio'
                ,'Corte fuerte'
                ,'Insonorizar'
                ,'Pararrayos'
                ,'Cura natural'
                ,'Madrugar'
                ,'Nado rápido'
                ,'Iluminación'
                ,'Cuerpo llama'
                ,'Absorbe electricidad'
                ,'Calco'
                ,'Armadura batalla'
                ,'Presión'
                ,'Entusiasmo'
                ,'Robustez'
                ,'Impulso'
                ,'Sombra trampa'
                ,'Dicha'
                ,'Ventosas'
                ,'Ritmo propio'
                ,'Chorro arena'
                ,'Pereza'
                ,'Superguarda'
                ,'Energía pura'
                ,'Más'
                ,'Menos'
                ,'Viscosecreción'
                ,'Piel tosca'
                ,'Velo agua'
                ,'Escudo magma'
                ,'Humo blanco'
                ,'Inmunidad'
                ,'Escama especial'
                ,'Predicción'
                ,'Cambio color'
                ,'Llovizna'
                ,'Sequía'
                ,'Esclusa de aire'
                ,'Simple'
                ,'Rivalidad'
                ,'Rompemoldes'
                ,'Recogemiel'
                ,'Don floral'
                ,'Viscosidad'
                ,'Experto'
                ,'Detonación'
                ,'Impasible'
                ,'Anticipación'
                ,'Nevada'
                ,'Electromotor'
                ,'Defensa hoja'
                ,'Manto níveo'
                ,'Adaptable'
                ,'Inicio lento'
                ,'Hidratación'
                ,'Mal sueño'
                ,'Multitipo'
                ,'Tinovictoria'
                ,'Gula'
                ,'Alerta'
                ,'Sacapecho'
                ,'Ignorante'
                ,'Ímpetu arena'
                ,'Alma cura'
                ,'Bromista'
                ,'Piel milagro'
                ,'Momia'
                ,'Roca sólida'
                ,'Flaqueza'
                ,'Ilusión'
                ,'Cacheo'
                ,'Funda'
                ,'Gélido'
                ,'Punta acero'
                ,'Telepatía'
                ,'Puño férreo'
                ,'Competitivo'
                ,'Audaz'
                ,'Justiciero'
                ,'Turbollama'
                ,'Terravoltaje'
                ,'Descarga'
                ,'Velo flor'
                ,'Herbívoro'
                ,'Pelaje recio'
                ,'Indefenso'
                ,'Velo dulce'
                ,'Respondón'
                ,'Garra dura'
                ,'Megadisparador'
                ,'Piel seca'
                ,'Mandíbula fuerte'
                ,'Piel helada'
                ,'Carrillo'
                ,'Aura feérica'
                ,'Aura oscura'
                ,'Prestidigitador'
                ,'Vigilante'
                ,'Batería'
                ,'Ensañamiento'
                ,'Pompa'
                ,'Corrosión'
                ,'Peluche'
                ,'Receptor'
                ,'Huida'
                ,'Retirada'
                ,'Hidrorrefuerzo'
                ,'Revés'
                ,'Sistema alfa'
                ,'Letargo perenne'
                ,'Cuerpo vívido'
                ,'Cólera'
                ,'Acero templado'
                ,'Antibalas'
                ,'Guardia metálica'
                ,'Guardia espectro'
                ,'Ultraimpulso'
                ,'Armadura prisma'
                ,'Coránima'
                ,'Pelusa'
                ,'Recogebolas'
                ,'Combustible'
                ,'Maduración'
                ,'Expulsarena'
                ,'Tragamisil'
                ,'Cobardía'
                ,'Armadura frágil'
                ,'Alma errante'
                ,'Fuente energía'
                ,'Potencia bruta'
                ,'Metal liviano'
                ,'Espada indómita'
                ,'Escudo recio'
                ,'Transistor'
                ,'Mandíbula dragón'
                ,'Relincho blanco'
                ,'Relincho negro'
                ,'Nerviosismo'
                ,'Velo aroma'
                ,'Compiescolta'
                ,'Cuerpo horneado'
                ,'Disemillar'
                ,'Sal purificadora'
                ,'Dinamo'
                ,'Energía eólica'
                ,'Liviano'
                ,'Surcavientos'
                ,'Poder fúngico'
                ,'Coraza ira'
                ,'Oportunista'
                ,'Baba'
                ,'Cambio heroico'
                ,'Geofagia'
                ,'Capa tóxica'
                ,'Intrépido'
                ,'Comandar'
                ,'Rumia'
                ,'Termoconversión'
                ,'Cuerpo áureo'
                ,'Tablilla debacle'
                ,'Espada debacle'
                ,'Caldero debacle'
                ,'Abalorio debacle'
                ,'Latido oricalco'
                ,'Motor hadrónico'
                ,'Cambio táctico'
                ,'Tenacidad'
                ,'Paleosíntesis'
                ,'Unidad ecuestre'
                ,'Monotema'
                ,'Néctar dulce'
                ,'Carga cuark'
                ,'Cadena tóxica'
                ,'Toque tóxico'
                ,'Cuerpo maldito'
                ,'Disfraz'
                ,'Mutapetito'
                ,'Tumbos'
                ,'Hospitalidad'
                ,'Cola surf'
                ,'Mano rápida'
                ,'Medicina extraña'
                ,'Mimetismo'
                ,'Herbogénesis'
                ,'Nebulogénesis'
                ,'Electrogénesis'
                ,'Psicogénesis'
                ,'Cara de hielo'
                ,'Poder arena'
                ,'Regeneración'
                ,'Olor persistente'
                ,'Pareja de baile'
                ,'Punk rock'
                ,'Puño invisible'
                ,'Banco'
                ,'Tumbos'
                ,'Agallas'
                ,'Mudar'
                ,'Rivalidad'
                ,'Muro mágico'
                ,'Tenacidad'
                ,'Piel seca'
                ,'Cromolente'
                ,'Trampa arena'
                ,'Experto'
                ,'Aclimatación'
                ,'Irascible'
                ,'Absorbe fuego'
                ,'Humedad'
                ,'Fuerza mental'
                ,'Sincronía'
                ,'Indefenso'
                ,'Viscosecreción'
                ,'Robustez'
                ,'Ritmo propio'
                ,'Madrugar'
                ,'Hidratación'
                ,'Viscosidad'
                ,'Encadenado'
                ,'Alerta'
                ,'Caparazón'
                ,'Electricidad estática'
                ,'Pararrayos'
                ,'Audaz'
                ,'Puño férreo'
                ,'Gas reactivo'
                ,'Cabeza roca'
                ,'Dicha'
                ,'Defensa hoja'
                ,'Intrépido'
                ,'Francotirador'
                ,'Velo agua'
                ,'Cura natural'
                ,'Rompemoldes'
                ,'Adaptable'
                ,'Descarga'
                ,'Nado rápido'
                ,'Presión'
                ,'Inmunidad'
                ,'Vista lince'
                ,'Insomnio'
                ,'Iluminación'
                ,'Potencia'
                ,'Recogida'
                ,'Poder solar'
                ,'Ojo compuesto'
                ,'Absorbe agua'
                ,'Afortunado'
                ,'Fuga'
                ,'Velo arena'
                ,'Pies rápidos'
                ,'Punto tóxico'
                ,'Gula'
                ,'Escudo magma'
                ,'Manto níveo'
                ,'Entusiasmo'
                ,'Cacheo'
                ,'Impasible'
                ,'Cura lluvia'
                ,'Surcavientos'
                ,'Llovizna'
                ,'Calco'
                ,'Antídoto (habilidad)'
                ,'Sebo'
                ,'Normalidad'
                ,'Rezagado'
                ,'Corte fuerte'
                ,'Despiste'
                ,'Simple'
                ,'Roca sólida'
                ,'Sequía'
                ,'Anticipación'
                ,'Gélido'
                ,'Ignorante'
                ,'Intimidación'
                ,'Colector'
                ,'Liviano'
                ,'Zoquete'
                ,'Detonación'
                ,'Ignífugo'
                ,'Cortante'
                ,'Ímpetu arena'
                ,'Electromotor'
                ,'Armadura frágil'
                ,'Chorro arena'
                ,'Poder arena'
                ,'Regeneración'
                ,'Potencia bruta'
                ,'Toque tóxico'
                ,'Clorofila'
                ,'Allanamiento'
                ,'Autoestima'
                ,'Sacapecho'
                ,'Nevada'
                ,'Herbívoro'
                ,'Cuerpo maldito'
                ,'Nerviosismo'
                ,'Menos'
                ,'Cuerpo llama'
                ,'Quitanieves'
                ,'Flexibilidad'
                ,'Funda'
                ,'Fuerte afecto'
                ,'Carrillo'
                ,'Ventosas'
                ,'Mandíbula fuerte'
                ,'Polvo escudo'
                ,'Espíritu vital'
                ,'Firmeza'
                ,'Efecto espora'
                ,'Regia presencia'
                ,'Primer auxilio'
                ,'Telepatía'
                ,'Insonorizar'
                ,'Fuerza cerebral'
                ,'Humo blanco'
                ,'Garra dura'
                ,'Metal pesado'
                ,'Antibalas'
                ,'Absorbe electricidad'
                ,'Perro guardián'
                ,'Hurto'
                ,'Cobardía'
                ,'Cola armadura'
                ,'General supremo'
                ,'Rizos rebeldes'
                ,'Filtro'
                ,'Antibarrera'
                ,'Velo pastel'
                ,'Más'
                ,'Ojo mental'
                ,'Clorofila'
                ,'Poder solar'
                ,'Cura lluvia'
                ,'Fuga'
                ,'Cromolente'
                ,'Francotirador'
                ,'Sacapecho'
                ,'Entusiasmo'
                ,'Nerviosismo'
                ,'Pararrayos'
                ,'Ímpetu arena'
                ,'Potencia bruta'
                ,'Compiescolta'
                ,'Ignorante'
                ,'Sequía'
                ,'Cacheo'
                ,'Allanamiento'
                ,'Hedor'
                ,'Efecto espora'
                ,'Humedad'
                ,'Piel milagro'
                ,'Poder arena'
                ,'Nado rápido'
                ,'Competitivo'
                ,'Justiciero'
                ,'Muro mágico'
                ,'Impasible'
                ,'Gula'
                ,'Velo arena'
                ,'Cuerpo llama'
                ,'Regeneración'
                ,'Cálculo final'
                ,'Tumbos'
                ,'Gélido'
                ,'Toque tóxico'
                ,'Funda'
                ,'Armadura frágil'
                ,'Fuerza mental'
                ,'Detonación'
                ,'Cosecha'
                ,'Armadura batalla'
                ,'Liviano'
                ,'Aclimatación'
                ,'Audaz'
                ,'Alma cura'
                ,'Piel seca'
                ,'Espíritu vital'
                ,'Autoestima'
                ,'Cobardía'
                ,'Hidratación'
                ,'Impostor'
                ,'Anticipación'
                ,'Pies rápidos'
                ,'Agallas'
                ,'Manto níveo'
                ,'Escama especial'
                ,'Multiescamas'
                ,'Defensa hoja'
                ,'Absorbe fuego'
                ,'Puño férreo'
                ,'Absorbe agua'
                ,'Afortunado'
                ,'Espejo mágico'
                ,'Más'
                ,'Herbívoro'
                ,'Llovizna'
                ,'Encadenado'
                ,'Madrugar'
                ,'Bromista'
                ,'Telepatía'
                ,'Inmunidad'
                ,'Intimidación'
                ,'Metal liviano'
                ,'Respondón'
                ,'Hurto'
                ,'Recogemiel'
                ,'Sebo'
                ,'Veleta'
                ,'Insomnio'
                ,'Velo agua'
                ,'Impulso'
                ,'Rivalidad'
                ,'Ojo compuesto'
                ,'Ritmo propio'
                ,'Intrépido'
                ,'Experto'
                ,'Metal pesado'
                ,'Menos'
                ,'Absorbe electricidad'
                ,'Presión'
                ,'Irascible'
                ,'Caparazón'
                ,'Ímpetu tóxico'
                ,'Adaptable'
                ,'Colector'
                ,'Gran encanto'
                ,'Mutatipo'
                ,'Cuerpo maldito'
                ,'Despiste'
                ,'Robustez'
                ,'Tenacidad'
                ,'Insonorizar'
                ,'Ímpetu ardiente'
                ,'Flexibilidad'
                ,'Vista lince'
                ,'Piel tosca'
                ,'Antídoto (habilidad)'
                ,'Espesura'
                ,'Mar llamas'
                ,'Torrente'
                ,'Simple'
                ,'Rompemoldes'
                ,'Zoquete'
                ,'Sombra trampa'
                ,'Dicha'
                ,'Electromotor'
                ,'Indefenso'
                ,'Enjambre'
                ,'Cuerpo puro'
                ,'Humo blanco'
                ,'Pereza'
                ,'Antibalas'
                ,'Prestidigitador'
                ,'Potencia'
                ,'Alas vendaval'
                ,'Simbiosis'
                ,'Manto frondoso'
                ,'Velo aroma'
                ,'Cabeza roca'
                ,'Nevada'
                ,'Piel feérica'
                ,'Baba'
                ,'Remoto'
                ,'Voz fluida'
                ,'Recogida'
                ,'Velo dulce'
                ,'Cura natural'
                ,'Herbogénesis'
                ,'Líbero'
                ,'Coraza reflejo'
                ,'Vigilante'
                ,'Hélice caudal'
                ,'Alma acerada'
                ,'Cuerpo mortal'
                ,'Electrogénesis'
                ,'Escama de hielo'
                ,'Quitanieves'
                ,'Acérrimo'
                ,'Cortante'
                ,'Mudar'
                ,'Transportarrocas'
                ,'Inicio lento'
                ,'Filtro'
                ,'Corrosión'
                ,'Peluche'
                ,'Unísono'
                ,'Modo daruma'
                ,'Viscosidad'
                ,'Piel eléctrica'
                ,'Reacción química'
                ,'Ignífugo'
                ,'Nebulogénesis'
                ,'Psicogénesis'
                ,'Rumina'
            ];
           foreach($habilidades as $habilidad){
            $url = "https://www.wikidex.net/wiki/".str_replace(" ", "_", $habilidad);
            
            $info = $this->getInfo($url);
            $string = $info;
            $dom = new \DOMDocument();
            @$dom->loadHTML(html_entity_decode($string));

            if(count($dom->getElementsByTagName('dd')) > 0){
                $spanName = $dom->getElementsByTagName('dd');
                $spanName = $spanName[1]->nodeValue;
            
            }else{
                $spanName = $dom->getElementsByTagName('p');
                $spanName = $spanName[1]->nodeValue;
            }
            //obtener nombre
           

            if(count(DB::table('habilidades')->where('name', $habilidad)->get()) == 0){
                DB::insert(
                    'insert into habilidades ( name, effect) VALUES ( ?, ?)',
                     [
                        $habilidad,
                        $spanName   
                    ]);
            }
            log::debug($habilidad);
           }
            return "ok";
            
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }

    public function getListPokemon(){

        try{
            $pokemon = DB::table('pokemon')->get();
            //return $pokemon;
            $listaPokemon = [];
            ini_set('max_execution_time', 3600);        
            
            foreach($pokemon as $poke){
                $url = "https://www.wikidex.net/wiki/$poke->name";
                $info = $this->getInfo($url);
                preg_match('/<span id="numeronacional">(.*?)<\/span>/s', $info, $nPoke);
                
                $affected = DB::table('pokemon')->where('name', $poke->name)->update(['nPoke' => $nPoke[1]]);
                log::Debug($poke->name." - ".$nPoke[1]);
            }

            return 'ok';
        }catch(Exception $e){ 
            log::debug($e);
          
        }
    }*/

}
