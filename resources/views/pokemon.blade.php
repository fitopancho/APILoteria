<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    

    </head>
    <body>
        <div class="container">
            <div class="row" style="margin-top: 5rem">
                <div id="tipos" class="col-md-6 d-flex align-items-center flex-row-reverse" >

                @foreach ($data['tipo'] as $tipo)
                    <img src="https://fitopanchodev.cl/laravel8/public/assets/img/tipo_{{ $tipo->nombre}}.png" style="width: 5rem; height: 5rem; margin:5px" class="card-img-top p-2" >
                @endforeach

                </div>
                <div id="imagen" class="col-md-6">
                    <img src="{{ $data['sprite']}}" style="width: 18rem; margin:5px" class="card-img-top" >
                </div>
            </div>
            <div class="row">
                <div id="habilidades" class="col-md-12">
                    <h2>Habilidades</h2>
                    @foreach ($data['habilidades'] as $habilidades)
                        <b>{{$habilidades['nombre']}}</b>
                        <p>{{$habilidades['efecto']}}</p>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div id="debilidades" class="col-md-12">
                    <h2>Debilidades</h2>
                    <b>Daño x 0</b><br>
                    @foreach ($data['debilidades']['cero'] as $debilidadesCero)
                        <img src="https://fitopanchodev.cl/laravel8/public/assets/img/tipo_{{ $debilidadesCero->nombre}}.png" style="width: 5rem; height: 5rem; margin:2px" class="card-img-top " >
                    @endforeach
                    <br>
                    <b>Daño x 1/4</b><br>
                    @foreach ($data['debilidades']['cuarto'] as $debilidadesCuarto)
                        <img src="https://fitopanchodev.cl/laravel8/public/assets/img/tipo_{{ $debilidadesCuarto->nombre}}.png" style="width: 5rem; height: 5rem; margin:2px" class="card-img-top " >
                    @endforeach
                    <br>
                    <b>Daño x 1/2</b><br>
                    @foreach ($data['debilidades']['medio'] as $debilidadesMedio)
                        <img src="https://fitopanchodev.cl/laravel8/public/assets/img/tipo_{{ $debilidadesMedio->nombre}}.png" style="width: 5rem; height: 5rem; margin:2px" class="card-img-top " >
                    @endforeach
                    <br>
                    <b>Daño x 1</b><br>
                    @foreach ($data['debilidades']['normal'] as $debilidadesNormal)
                        <img src="https://fitopanchodev.cl/laravel8/public/assets/img/tipo_{{ $debilidadesNormal->nombre}}.png" style="width: 5rem; height: 5rem; margin:2px" class="card-img-top " >
                    @endforeach
                    <br>
                    <b>Daño x 2</b><br>
                    @foreach ($data['debilidades']['doble'] as $debilidadesDoble)
                        <img src="https://fitopanchodev.cl/laravel8/public/assets/img/tipo_{{ $debilidadesDoble->nombre}}.png" style="width: 5rem; height: 5rem; margin:2px" class="card-img-top " >
                    @endforeach
                    <br>
                    <b>Daño x 4</b><br>
                    @foreach ($data['debilidades']['cuatro'] as $debilidadesCuatro)
                        <img src="https://fitopanchodev.cl/laravel8/public/assets/img/tipo_{{ $debilidadesCuatro->nombre}}.png" style="width: 5rem; height: 5rem; margin:2px" class="card-img-top " >
                    @endforeach
                    
                </div>
            </div>
            <br>
            <div class="row">
                <h2>Características de combate</h2>
                <p>nivel 50, 31IV, 252 EV, Naturaleza favorable</p>
                <div id="caracteristicas" class="col-md-6 d-flex align-items-center flex-row-center">
                    
                    <table class="table table-striped">
                        <tr>
                            <th>Stat</th>
                            <th>Base</th>
                            <th>Min</th>
                            <th>Max</th>
                        </tr>
                        
                        @foreach ($data['stats'] as $stat)
                            <tr>
                                <td>{{ $stat['nm_stat']}}</td>
                                <td>{{ $stat['val_base']}}</td>
                                <td>{{ $stat['val_min']}}</td>
                                <td>{{ $stat['val_max']}}</td>
                            </tr>
                        @endforeach
                            
                        
                        
                        
                    </table>
                </div>
            </div>
            
        </div>
  </body>
  
</html>
