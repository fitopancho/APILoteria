<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Busca tu pokémon</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="exampleDataList" class="form-label">Por nombre</label>
                    <input class="form-control" list="datalistOptions" id="listaNombres" placeholder="Pikac..." onchange="getPokemonByName()">
                    <datalist id="datalistOptions">
                        
                    </datalist>
                </div>
            </div>
            <div class="row">
                <div class="row">
                    <div class="col-md-12">
                        <h1>o por características de batalla</h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row" style="background-color:#AA5343;">
                        
                        <div class="col-md-6">
                        <label  class="form-label">Hp Base mayor que</label>
                            <input class="form-control"  id="hpBasePoke1" style=" margin-bottom: 10px">
                            
                        </div>
                        <div class="col-md-6">
                        <label  class="form-label">Hp Base menor que</label>
                            <input class="form-control"  id="hpBasePoke2" style=" margin-bottom: 10px">
                            
                        </div>
                    </div>
                    <div class="row" style="background-color:#5BAFD1">
                        <div class="col-md-6">
                            <label  class="form-label">Ataque Base mayor que</label>
                            <input class="form-control"  id="atkBasePoke1" style=" margin-bottom: 10px">
                            
                        </div>
                        <div class="col-md-6">
                            <label  class="form-label">Ataque Base menor que</label>
                            <input class="form-control"  id="atkBasePoke2" style=" margin-bottom: 10px">
                            
                        </div>
                        
                        </div>
                    <div class="row" style="background-color:#E87282">

                        <div class="col-md-6">
                            <label  class="form-label">Defensa Base mayor que</label>
                            <input class="form-control"  id="defBasePoke1" style=" margin-bottom: 10px">
                            
                        </div>
                        <div class="col-md-6">
                            <label  class="form-label">Defensa Base menor que</label>
                            <input class="form-control"  id="defBasePoke2" style=" margin-bottom: 10px">
                            
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="row" style="background-color:#B4CC49">
                        
                        <div class="col-md-6">
                            <label  class="form-label">Ataque Especial Base mayor que</label>
                            <input class="form-control"  id="sAtBasePoke1" style=" margin-bottom: 10px">
                            
                        </div>
                        <div class="col-md-6">
                            <label  class="form-label">Ataque Especial Base menor que</label>
                            <input class="form-control"  id="sAtBasePoke2" style=" margin-bottom: 10px">
                            
                        </div>
                    </div>
                    <div class="row" style="background-color:#B99845">

                        <div class="col-md-6">
                            <label  class="form-label">Defensa Especial Base mayor que</label>
                            <input class="form-control"  id="sDfBasePoke1" style=" margin-bottom: 10px">
                            
                        </div>
                        <div class="col-md-6">
                            <label  class="form-label">Defensa Especial Base menor que</label>
                            <input class="form-control"  id="sDfBasePoke2" style=" margin-bottom: 10px">
                            
                        </div>
                    </div>
                    <div class="row" style="background-color:#EDF2FA">

                        <div class="col-md-6">
                            <label  class="form-label">Velocidad Base mayor que</label>
                            <input class="form-control"  id="velBasePoke1" style=" margin-bottom: 10px">
                            
                        </div>
                        <div class="col-md-6">
                            <label  class="form-label">Velocidad Base menor que</label>
                            <input class="form-control"  id="velBasePoke2" style=" margin-bottom: 10px">
                            
                        </div>
                    </div>
                </div>
                
            </div>
            <br>
            <button type="button" class="btn btn-primary" onclick="findPokemon()">Buscar</button>
            <br>
            <br>
            <div id="parentList">
                <div class="row" id="childList">

                </div>

            </div>
        </div>
  </body>
  <script>
    $( document ).ready(function() {
        $.get("https://fitopanchodev.cl/laravel8/public/api/nodo2/pokemon/list", function(data, status){
            
            document.getElementById("listaNombres").disabled = true;
            data.forEach((pokemon) => agregaOption(pokemon));
            document.getElementById("listaNombres").disabled = false;

            
        });
    });

    function agregaOption(pokemon) {
        var option =  document.createElement("option");
        option.value = pokemon;
        document.getElementById("datalistOptions").appendChild(option)
    }

    function getPokemonByName() {
        limpiaLista();
        
        $.get("https://fitopanchodev.cl/laravel8/public/api/web/pokemon/list/"+document.getElementById("listaNombres").value, function(data, status){
            
            data.forEach((pokemon) => agregaCardPokemon(pokemon));

        });
        
    }

    function findPokemon() {
        limpiaLista();

        var request = {
            "hpMin": document.getElementById("hpBasePoke1").value,
            "hpMax": document.getElementById("hpBasePoke2").value,
            "atkMin": document.getElementById("atkBasePoke1").value,
            "atkMax": document.getElementById("atkBasePoke2").value,
            "defMin": document.getElementById("defBasePoke1").value,
            "defMax": document.getElementById("defBasePoke2").value,
            "sAtkMin": document.getElementById("sAtBasePoke1").value,
            "sAtkMax": document.getElementById("sAtBasePoke2").value,
            "sDefMin": document.getElementById("sDfBasePoke1").value,
            "sDefMax": document.getElementById("sDfBasePoke2").value,
            "velMin": document.getElementById("velBasePoke1").value,
            "valMax": document.getElementById("velBasePoke2").value
        }

        $.post("https://fitopanchodev.cl/laravel8/public/api/web/pokemon/list",
        request,
        function(data, status){
            if(data.length == 0){
                var text =  document.createElement("p");
                text.appendChild(document.createTextNode("Sin resultados"));
                document.getElementById("childList").appendChild(text)
            }
            data.forEach((pokemon) => agregaCardPokemon(pokemon));
        });
        
        
        
        
    }

    function agregaCardPokemon(pokemon) {

        var a = document.createElement("a");
        a.setAttribute("href", "https://fitopanchodev.cl/laravel8/public/pokemon/"+pokemon);
        a.setAttribute("style", "width: 9rem; margin:5px");

        var divCard =  document.createElement("div");
        divCard.setAttribute("class", "card");
        divCard.setAttribute("style", "width: 9rem; margin:5px");

        var img =  document.createElement("img");
        img.setAttribute("class", "card-img-top");
        img.setAttribute("src", "https://fitopanchodev.cl/laravel8/public/assets/pokemon/"+pokemon+".png");
        img.setAttribute("width", "200px");

        var divCardBody =  document.createElement("div");
        divCardBody.setAttribute("class", "card-body");

        var h5Name =  document.createElement("h6");
        h5Name.setAttribute("class", "card-title");
        h5Name.appendChild(document.createTextNode(pokemon));

        
        divCardBody.appendChild(h5Name)

        divCard.appendChild(img)
        divCard.appendChild(divCardBody)
        a.appendChild(divCard)

        document.getElementById("childList").appendChild(a)
    }

    function limpiaLista(){
        var parentList = document.getElementById("parentList");
        var childList = document.getElementById("childList");
        parentList.removeChild(childList);

        childList =  document.createElement("div");
        childList.setAttribute("class", "row");
        childList.setAttribute("id", "childList");
        document.getElementById("parentList").appendChild(childList)
    }
  </script>
</html>
