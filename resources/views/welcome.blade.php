<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Rol Delay</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="  crossorigin="anonymous"></script>

    </head>
    <body>
        <div class="container ">
            
            <div class="card" style="width: 18rem; margin: 0 auto;float: none;margin-bottom: 10px; margin-top: 20%">
                <div class="card-body">
                <h1 class="card-title">Rol delay</h1>
                    <div class="row">
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#loginModal">Iniciar Sesión</button>
                    </div>
                    <hr>
                    <div class="row">
                        <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#registerModal">Registrarse</button>
                    </div>
                </div>
            </div> 
        </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Iniciar sesión</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="email" aria-label="email" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" placeholder="password" aria-label="password" aria-describedby="basic-addon1">
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="registerModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registrarse</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" id="nm_reg" placeholder="nombre" aria-label="nombre" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <input type="text" class="form-control" id="email_reg" placeholder="email" aria-label="email" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" id="pass_reg" placeholder="password" aria-label="password" aria-describedby="basic-addon1">
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" id="pass_reg_rep" placeholder="repita password" aria-label="password" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onClick="registro()" data-bs-dismiss="modal">Enviar</button>
            </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="MensajesModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Atención</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p id="p_mensaje"></p>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-primary" data-bs-dismiss="modal">OK</button>
            </div>
            </div>
        </div>
    </div>
    <button type="button" id="mensajeButton" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#MensajesModal" hidden></button>
  </body>
  <script>
    function registro(){
        nm_reg = document.getElementById("nm_reg").value;
        email_reg = document.getElementById("email_reg").value;
        pass_reg = document.getElementById("pass_reg").value;
        pass_reg_rep = document.getElementById("pass_reg_rep").value;
        if(nm_reg == '' || email_reg == '' || pass_reg == '' || pass_reg_rep == ''){
            mensaje("debe rellenar todos los campos")
        }else{
            if(pass_reg != pass_reg_rep){
                mensaje("La contraseña no coincide")
            }else{
                registrar(nm_reg, email_reg, pass_reg)
            }
        }
        
    }

    function mensaje(mensaje){
        document.getElementById("p_mensaje").innerText = mensaje ;
        document.getElementById("mensajeButton").click();
    }

    function registrar(nm_reg, email_reg, pass_reg){
        $.ajax({
            type:"POST", 
            url:"api/jugador/registrar", 
            data:{
                name: nm_reg,
                email: email_reg,
                password: pass_reg
            }, 
            success:function(datos){ 
                mensaje("usuario creado con éxito")
            },
            error:function(errors){
                if(errors.responseJSON.errors.email[0] == "The email field must be a valid email address."){
                    mensaje("El correo electrónico debe tener un formato valido.")
                }
                if(errors.responseJSON.errors.password[0] == "The password field must be at least 6 characters."){
                    mensaje("La contraseña debe tener almenos 6 caracteres.")
                }
                
            }
           
        })
    }
  </script>
</html>
